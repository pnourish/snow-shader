﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowLayer : MonoBehaviour {
    [Range(512, 4096)]
    public int TextureSize = 1024;

    [Range(0, 10000)]
    public int SnowFallPerSecond = 10;

    [Range(0, 1.0f)]
    public float SnowHeightMultiplier = 0.001f;

    public Texture2D snowCoverageTexture;


    private float snowFallTimer = 0.0f;
    private Color snowColor = new Color(0.10f, 0.10f, 0.10f);
    private Color noSnowColor = new Color(0.10f, 0.10f, 0.10f);


    // Use this for initialization
    void Start () {
        snowCoverageTexture = new Texture2D(TextureSize, TextureSize);

        var pixels = snowCoverageTexture.GetPixels();
        var numPixels = TextureSize * TextureSize;
        for (int i = 0;i < numPixels;++i)
        {
            pixels[i].r = 0.0f;
            pixels[i].g = 0.0f;
            pixels[i].b = 0.0f;
        }
        snowCoverageTexture.SetPixels(pixels);
        snowCoverageTexture.Apply();

        //GetComponent<Renderer>().material.SetTexture("_Diffuse", snowCoverageTexture);

        GetComponent<Renderer>().material.SetTexture("_HeightMap", snowCoverageTexture);


    }

    // Update is called once per frame
    void Update () {
        float timeBetweenSnowFall = 1.0f / SnowFallPerSecond;
        snowFallTimer += Time.deltaTime;

        if (snowFallTimer > timeBetweenSnowFall)
        {
            ChangeSnowValue(timeBetweenSnowFall, snowColor);
        }


        //ChangeSnowValue(timeBetweenSnowFall,noSnowColor);



        GetComponent<Renderer>().material.SetFloat("_SnowAccumulationLevel", SnowHeightMultiplier);

    }

    public Texture2D GetSnowCoverage() { return snowCoverageTexture; }

    void ChangeSnowValue(float timeBetweenSnowFall, Color colorToSet)
    {
        int blockSize = 30;

            int x = Random.Range(0, TextureSize - blockSize);
            int y = Random.Range(0, TextureSize - blockSize);
            //snowCoverageTexture.SetPixel(x, y, noSnowColor);

        Color[] currentPixels = snowCoverageTexture.GetPixels(x, y, blockSize, blockSize);
        var numPixels = blockSize * blockSize;
        for (int i = 0; i < numPixels; ++i)
        {
            currentPixels[i].r = currentPixels[i].r + colorToSet.r;
            currentPixels[i].g = currentPixels[i].g + colorToSet.g;
            currentPixels[i].b = currentPixels[i].b + colorToSet.b;
        }

        snowCoverageTexture.SetPixels(x, y, blockSize, blockSize, currentPixels );


        snowCoverageTexture.Apply();
    }
}
