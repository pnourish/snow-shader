﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowLevelChangeScript : MonoBehaviour {


    Renderer shaderRenderer;

    public float snowIncreaseAmount;
    public float snowDecreaseAmount;
    float currSnowAmount;
    // Use this for initialization
    void Start ()
    {
        snowIncreaseAmount = 0.003f;
        snowDecreaseAmount = -0.001f;
        shaderRenderer = GetComponent<Renderer>();

    }

    // Update is called once per frame
    void Update ()
    {
        ChangeSnowLevel(snowDecreaseAmount);
        Debug.Log("Decrease Snow");

    }

    void ChangeSnowLevel(float changeAmount)
    {
        /*
        float currSnowAmount = shaderRenderer.sharedMaterial.GetFloat("_SnowAccumulationLevel") + changeAmount;
        currSnowAmount = Mathf.Clamp01(currSnowAmount);
        Debug.Log("Curr snow amount is: " + currSnowAmount);
        shaderRenderer.sharedMaterial.SetFloat("_SnowAccumulationLevel",currSnowAmount);
        */
        var snow = GetComponent<SnowLayer>();
        if(snow) snow.SnowFallPerSecond += 1;
    }


    void OnParticleCollision(GameObject other)
    {
        ParticleSystem particleSystem = other.GetComponent<ParticleSystem>();
        ParticleCollisionEvent[] collisions = new ParticleCollisionEvent[particleSystem.safeCollisionEventSize];
        int numberOfCollisions = particleSystem.GetCollisionEvents(this.gameObject, collisions);
        

        for (int i = 0; i < numberOfCollisions; i++)
        {
            Debug.Log("Increase Snow");
            ChangeSnowLevel(snowIncreaseAmount);
           
        }
    }
}
